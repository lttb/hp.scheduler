# HP.Scheduler

HP.Scheduler is a lightweight task scheduler application for Node JS with REST HTTP API supporting.

  - Mongo backed persistance layer
  - HAProxy as a balancer
  - PM2 as a process manager
  - REST API
  - Extended CRON support (with seconds and years)
  - Package Task POSTing
  - Task updating on fly

## Installation

You need to install Mongo, HAProxy 1.6+, npm, NodeJS and PM2.

Run bash script to set HAProxy configs and build app:

```
$ bash init.sh
```

## Usage

Start app-server:

```
$ node .
```

Start app with PM2:

```
$ pm2 start . --name="Scheduler"
```

Start app in cluster:

```
$ pm2 start pm2.json
```

You can define workers in pm2.json.


### API

App accepts extended CRON expressions, so you can use something like:
  
  - 00 48 16 26 1 ? 2016
  - 00 45 16
  - 5/6
  - */3 20-30 * L Jan-Feb Sun-Sat 2016,2018

**Note:** always the first seconds, then minutes

There are some API examples.

##### GET

Get all running or stopped tasks on the server:

```
GET http://localhost/command/
```

Get all done tasks:

```
GET http://localhost/command/?status=done
```

Get task by ID:

```
GET http://localhost/command/56a77830b8db65e541c3c9a5
```

##### POST

Create new task:

```
POST http://localhost/command/ { request, schedule }
```

Create new tasks in package:

```
POST http://localhost/command/ [{ request, schedule }, { request, schedule }]
```

##### PUT

Manage task:

```
PUT http://localhost/command/56a77830b8db65e541c3c9a5 { request, schedule, status }
```

For example, you can stop the task for a while:

```
PUT http://localhost/command/56a77830b8db65e541c3c9a5 { "status": "stopped" }
```

And start it then:

```
PUT http://localhost/command/56a77830b8db65e541c3c9a5 { "status": "running" }
```

**Note:** App has a watcher that clean outdated stopped tasks

##### DELETE

Delete task:

```
DELETE http://localhost/command/56a77830b8db65e541c3c9a5
```