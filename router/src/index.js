import express from 'express';
import bodyParser from 'body-parser';
import commandRouter from './command-router';
import { appPort } from './config';

const app = express();

app.use(bodyParser.json());

app.use('/command', commandRouter);

app.listen(appPort);
