import mongoose from 'mongoose';
import Redis from 'ioredis';

import { mongoDbAddress, redisAddress } from './../common/config';
import { taskSchema } from './../common/schemas';
import dbModels from './../common/dbModels';
import { getError, isValidTask } from './../common/tasks-utils';

import RequestReply from './redis-request-reply';

export default { read, create, update, remove };

const db = mongoose.createConnection(`mongodb://${mongoDbAddress}/tasks`);
dbModels.define('Task', taskSchema, db);

//dbModels.Task.remove().exec();

const redisPusher = new Redis(redisAddress);
const redisPopper = new Redis(redisAddress);

const reqRepWithWorker = new RequestReply(redisPusher, redisPopper);

/**
 * Get tasks with filter or task by id
 * @param  {String} taskId
 * @param  {Object} searchingParams Params for filter
 * @return {Promise}
 */
function read(taskId = null, searchingParams = {}) {
  const result = taskId !== null
    ? getById(taskId)
    : getByParams(searchingParams);
  return Promise
    .resolve(result)
    .then(data => data || getError(notFound));
}

/**
 * Create new task
 * @param  {String} options.schedule as Cron expr
 * @param  {String} options.request
 * @return {Promise}
 */
function create(task) {
  if (Array.isArray(task)) {
    return task.map(create);
  }
  const { request, schedule } = task;
  const taskCreatedAt = new Date();
  const newTask = {
    request,
    schedule,
    createdAt: taskCreatedAt,
    updatedAt: taskCreatedAt,
    _id: generateNewTaskId(),
  };
  const taskValidation = isValidTask(newTask);
  if (taskValidation !== true) {
    return taskValidation;
  }
  redisPusher.lpush('newTasks', JSON.stringify(newTask));
  return { taskId: newTask._id };
}

/**
 * Update task by ID
 * @param  {String} taskId
 * @param  {Object} updateParams
 * @return {Promise}
 */
function update(taskId, updateParams) {
  return getById(taskId)
    .then(task => {
      if (!task) {
        return getError('notFound');
      }
      return reqRepWithWorker(task.workerAddress, task._id)('update', task, updateParams);
    });
}

/**
 * Remove task by ID
 * @param  {String} taskId
 * @return {Promise}
 */
function remove(taskId) {
  return getById(taskId)
    .then(task => {
      if (!task) {
        return getError('notFound');
      }
      return reqRepWithWorker(task._doc.workerAddress, taskId)('remove', taskId);
    });
}

/**
 * get taskId
 * @return {String}
 */
function generateNewTaskId() {
  return new mongoose.Types.ObjectId().toString();
}

/**
 * get Task by Id
 * @param  {String} taskId
 * @return {Promise|Object}
 */
function getById(taskId) {
  return dbModels.Task.findById(taskId).exec();
}

/**
 * get Tasks by params
 * @param  {Object.<String, String|Object} params
 * @return {Promise|Object}
 */
function getByParams(params) {
  return dbModels.Task.find(params).exec();
}
