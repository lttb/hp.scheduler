import zmq from 'zmq';

const zmqRequester = zmq.socket('req');

/**
 * Send request and get reply from worker by address:port
 * @param  {String} address 0.0.0.0:1111
 * @param  {Object|String} task
 * @param  {String} method
 * @return {Promise}
 */
export const reqRepWithWorker = (address, method, params) => new Promise(resolve => {
  zmqRequester.connect(`tcp://${address}`);
  zmqRequester.on('message', message => {
    zmqRequester.disconnect(`tcp://${address}`);
    resolve(JSON.parse(message.toString()));
  });
  zmqRequester.send(JSON.stringify({ params, method }));
});
