export default function init(redisPusher, redisPopper) {
  return (reqChannel, repChannel) => (method, ...params) => {
    return new Promise(resolve => {
      redisPopper.brpop(repChannel, 0).then(([, result]) => {
        resolve(JSON.parse(result));
      });
      redisPusher.lpush(reqChannel, JSON.stringify({ method, params, repChannel }));
    });
  };
}
