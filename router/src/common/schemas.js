export const taskSchema = {
  _id: String,
  request: Object,
  schedule: String,
  status: String,
  workerAddress: String,
  createdAt: { type: Date, default: new Date() },
  updatedAt: { type: Date, default: new Date() },
};
