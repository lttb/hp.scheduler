import ERRORS from './errors';
import STATUS from './status';

export function getError(error) {
  return ERRORS[error] || { error: 'unknown error', code: 500 };
}

/**
 * Task Params validation
 * @param  {Object} params
 * @return {Array}  array [ key, value ] of valid params
 */
export function validateParams(params) {
  return Object
    .entries(params)
    .filter(([param]) => isAvailableParam(param));
}

/**
 * check is it possible to update this Task param
 * @param  {String}  param
 * @return {Boolean}
 */
function isAvailableParam(param) {
  return ['status', 'schedule', 'request'].indexOf(param) !== -1;
}

/**
 * validate task
 * @param  {Object}  task
 * @return {Boolean|Error}
 */
export function isValidTask(task) {
  if (!(task.schedule && task.request)) {
    return getError('invalidArguments');
  }

  if (!isValidCronExpr(task.schedule)) {
    return getError('invalidCRON');
  }

  const acceptableStatuses = [STATUS.running, STATUS.stopped];
  if (task.status && acceptableStatuses.indexOf(task.status) === -1) {
    return { ...getError('unacceptableStatus'), msg: { acceptableStatuses } };
  }

  return true;
}

/**
 * validate Cron expression.
 * @param  {String}  expr
 * @return {Boolean}
 */
function isValidCronExpr(expr) {
  const periods = expr.split(' ');
  const periodRegExps = [
    /^[0-9\*]*[,\-\/]?[0-9]*?$/i,
    /^[0-9\*]*[,\-\/]?[0-9]*?$/i,
    /^[0-9\*]*[,\-\/]?[0-9]*?$/i,
    /^[0-9\*\?]*[,\-\/LW]?[0-9]*?$/i,
    /^[0-9a-z\*]*[,\-\/]?[0-9a-z]*?$/i,
    /^[0-9a-z\*\?]*[,\-\/L#]?[0-9a-z]*?$/i,
    /^[0-9\*]*[,\-\/]?[0-9]*?$/i,
  ];
  return periods.length - 1 <= periodRegExps.length && periods.reduce((acc, period, index) =>
    acc ? periodRegExps[index].test(period) : acc, true);
}
