import { Router } from 'express';
import Scheduler from './Scheduler';
import { setJsonHeader, Reply } from './utils';

const jsonReply = new Reply(setJsonHeader);

const router = new Router();

router.get('/:id?', (req, res) =>
  jsonReply(Scheduler.read(req.params.id, req.query), res)
);

router.post('/', (req, res) =>
  jsonReply(Scheduler.create(req.body), res)
);

router.put('/:id', (req, res) =>
  jsonReply(Scheduler.update(req.params.id, req.body), res)
);

router.delete('/:id', (req, res) =>
  jsonReply(Scheduler.remove(req.params.id), res)
);

export default router;
