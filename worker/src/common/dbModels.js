export default {
  define(name, schema, database) {
    this[name] = database.model(name, schema);
  },
};
