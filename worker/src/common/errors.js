/**
 * Possible errors Dictionary
 * @type {Object.<String, Object>}
 */
export default {
  notFound: { error: 'not found', code: 404 },
  invalidArguments: { error: 'invalid arguments', code: 400 },
  invalidCRON: { error: 'invalid CRON expression', code: 400 },
  outOfDate: { error: 'out of date', code: 400 },
  unacceptableStatus: { error: 'unacceptable status', code: 400 },
  db: {error: 'database error', code: 500 },
};
