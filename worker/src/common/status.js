/**
 * Index of available task statuses
 * @type {Object}
 */
export default {
  running: 'running',
  stopped: 'stopped',
  done: 'done',
  deleted: 'deleted',
  outdated: 'outdated',
};
