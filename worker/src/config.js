import os from 'os';

export const appPort = process.env.PORT || 4001;
export const workerIp = os.networkInterfaces().wlan0[0].address;
export const workerAddress = `${workerIp}:${appPort}`;
