import later from 'later';

later.date.localTime();

export default { start, stop, has, remove, setStopped };

const schedulers = new Map();

function start(task, perform, callbackOnDone = null) {
  const cronSchedule = getCronSchedule(task.schedule);
  if (!isValidCronSchedule(cronSchedule)) {
    throw new Error('outOfDate');
  }

  stop(task._id);

  schedulers.set(task._id, later.setInterval(() => {
    if (isValidCronSchedule(cronSchedule)) {
      perform(task.request);
    } else if (callbackOnDone) {
      callbackOnDone(task);
    }
  }, cronSchedule));

  return true;
}

function setStopped(taskId) {
  stop(taskId);
  schedulers.set(taskId, null);
}

function stop(taskId) {
  if (schedulers.has(taskId) && schedulers.get(taskId)) {
    schedulers.get(taskId).clear();
  }
}

function remove(taskId) {
  stop(taskId);
  schedulers.delete(taskId);
}

function has(taskId) {
  return schedulers.has(taskId);
}

function getCronSchedule(cron) {
  return later.parse.cron((cron + ' ').replace('* ', '*/1 '), true);
}

function isValidCronSchedule(cronSchedule) {
  return later.schedule(cronSchedule).next() !== 0;
}
