import mongoose from 'mongoose';
import Redis from 'ioredis';

import { workerAddress } from './../config';

import { mongoDbAddress, redisAddress } from './../common/config';
import { taskSchema } from './../common/schemas';
import dbModels from './../common/dbModels';
import { getError, isValidTask, validateParams } from './../common/tasks-utils';
import STATUS from './../common/status';

import Scheduler from './scheduler';
import { send as sendRequest } from './request';

import { interval } from './../utils';

const redisPopper = new Redis(redisAddress);
const redisPusher = new Redis(redisAddress);

const db = mongoose.createConnection(`mongodb://${mongoDbAddress}/tasks`);

const workerMethods = { create, read, update, remove };
const workerChannelsLookup = {
  newTasks: task => create(task),
  [workerAddress]: ({ params, method, repChannel }) => Promise
    .resolve(workerMethods[method](...params))
    .then(result => redisPusher.lpush(repChannel, JSON.stringify(result)))
    .catch(() => getError('unknown')),
};
const waitForNewTask = () => redisPopper
  .brpop(Object.keys(workerChannelsLookup), 0)
  .then(([channel, message]) => {
    workerChannelsLookup[channel](JSON.parse(message));
    waitForNewTask();
  });

if (process.env.NODE_ENV === 'test') {
  workerMethods.set = set;
  dbModels.define('Task', taskSchema, db.useDb('tests'));
  dbModels.Task.remove().exec();
} else {
  dbModels.define('Task', taskSchema, db);
  (async () => {
    await initTasks(dbModels.Task);
    await waitForNewTask();
    interval(tasksCleaner, 5 * 60 * 1000);
  })();
}

export default workerMethods;

/**
 * Get tasks with filter or task by id
 * @param  {String} taskId
 * @param  {Object} searchingParams Params for filter
 * @return {Promise}
 */
function read(taskId = null, searchingParams = {}) {
  return taskId !== null
    ? getById(taskId)
    : getByParams({ ...searchingParams, workerAddress });
}

/**
 * Create new task
 * @param  {String} options.schedule as Cron expr
 * @param  {String} options.request
 * @return {Promise}
 */
function create(newTask) {
  if (!newTask) {
    return getError('invalidArguments');
  }
  newTask.workerAddress = workerAddress;
  newTask.status = STATUS.running;
  return set(newTask, createdTask => {
    new dbModels.Task(createdTask).save();
  });
}

/**
 * Update task
 * @param  {String} taskId
 * @param  {Object} updateParams
 * @return {Promise}
 */
function update(task, updateParams) {
  if (!Scheduler.has(task._id)) {
    return getError('notFound');
  }
  const validatedParams = validateParams(updateParams);
  if (!validatedParams.length) {
    return getError('invalidArguments');
  }
  const newTask = validatedParams.reduce((acc, [param, value]) => ({
    ...acc,
    [param]: value,
  }), task);

  return set(newTask);
}

/**
 * Remove task
 * @param  {String} taskId
 * @return {Promise}
 */
function remove(taskId, status = STATUS.deleted) {
  if (!Scheduler.has(taskId)) {
    return getError('notFound');
  }
  Scheduler.remove(taskId);
  return dbTaskUpdate(taskId, { status }).then(() => `Task with id: ${taskId} has ben removed`);
}

/**
 * Set task
 * @param {Object}   task
 * @param {Function} callback
 * @return {Error|callbackResult|True}
 */
function set(task, callback = defaultCallbackOnTaskUpdate) {
  const taskValidation = isValidTask(task);
  if (taskValidation !== true) {
    return taskValidation;
  }

  task.updatedAt = new Date();

  if (task.status === STATUS.stopped) {
    Scheduler.setStopped(task._id);
  } else {
    try {
      Scheduler.start(task, sendRequest, taskOnDone);
    } catch (e) {
      return getError(e.message);
    }
  }

  if (callback) {
    return callback(task);
  }

  return true;
}

function defaultCallbackOnTaskUpdate(task) {
  return dbTaskUpdate(task._id, task).then(() => `Task with id: ${task._id} has been updated`);
}

/**
 * get Task by Id
 * @param  {String} taskId
 * @return {Promise|Object}
 */
function getById(taskId) {
  return dbModels.Task.findById(taskId).exec();
}

/**
 * get Tasks by params
 * @param  {Object.<String, String|Object} params
 * @return {Promise|Object}
 */
function getByParams(params) {
  return dbModels.Task.find(params).exec();
}

/**
 * removes outdated stopped tasks
 * @return {Promise}
 */
function tasksCleaner() {
  return read(null, { status: STATUS.stopped })
    .then(stoppedTasks => stoppedTasks.forEach(task => {
      const cronSchedule = getCronSchedule(task.schedule);
      if (!isValidCronSchedule(cronSchedule)) {
        remove(task._id, STATUS.outdated);
      }
    }));
}

/**
 * Get running and stopped tasks from the DB,
 * push them into the tasks collection
 */
function initTasks(taskModel) {
  return taskModel
    .find({
      $and: [
        { workerAddress },
        { $or: [{ status: STATUS.running }, { status: STATUS.stopped }] },
      ],
    })
    .exec()
    .then(activeTasks => activeTasks.map(task => {
      const result = set(task._doc, null);
      if (result !== true) {
        dbTaskUpdate(task._id, { status: STATUS.outdated });
      }
      return result;
    }));
}

/**
 * describe callback for done task
 * @param  {Object} task
 * @return {Promise}
 */
function taskOnDone(task) {
  return dbTaskUpdate(task._id, { status: STATUS.done });
}

function dbTaskUpdate(_id, $set, callback = null) {
  return dbModels.Task.update({ _id }, { $set }).exec(callback);
}
