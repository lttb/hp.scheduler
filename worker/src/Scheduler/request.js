import fs from 'fs';

/**
 * This is a mock-up for request Sender for debug
 */
export function send(command) {
  fs.appendFile('./output/test.txt', `sending request: ${new Date().getSeconds() + command}\n`);
}
