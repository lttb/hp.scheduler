import test from 'blue-tape';
import Scheduler from './../Scheduler';

test('test', t => {
  t.test('test invalid Scheduler without all arguments', assert => {
    const expected = { error: 'invalid arguments', code: 400 };
    const actual = Scheduler.set({});
    assert.deepEqual(actual, expected);
    return assert.end();
  });

  t.test('test invalid Scheduler without command', assert => {
    const expected = { error: 'invalid arguments', code: 400 };
    const actual = Scheduler.set({ schedule: '* * * * * *' });
    assert.deepEqual(actual, expected);
    return assert.end();
  });

  t.test('test invalid Scheduler without cron', assert => {
    const expected = { error: 'invalid arguments', code: 400 };
    const actual = Scheduler.set({ request: 'test' });
    assert.deepEqual(actual, expected);
    return assert.end();
  });

  t.test('test invalid cron', assert => {
    const expected = { error: 'invalid CRON expression', code: 400 };
    const actual = Scheduler.set({ schedule: 'abs', request: 'test' });
    assert.deepEqual(actual, expected);
    return assert.end();
  });

  t.test('test valid cron', assert => {
    const expected = true;
    const actual = Scheduler.set({ schedule: '* * * * * *', request: 'test', status: 'stopped' }, null);
    assert.deepEqual(actual, expected);
    return assert.end();
  });

  t.test('test Scheduler with invalid status', assert => {
    const expected = { msg: { acceptableStatuses: [ 'running', 'stopped' ] }, error: 'unacceptable status', code: 400 };
    const actual = Scheduler.set({ status: 'deleted', schedule: '* * * * * *', request: 'test' });
    assert.deepEqual(actual, expected);
    return assert.end();
  });

  t.test('test outdated cron', assert => {
    const expected = { error: 'out of date', code: 400 };
    const actual = Scheduler.set({ schedule: '* * * * * * 2015', request: 'test' });
    assert.deepEqual(actual, expected);
    return assert.end();
  });

  t.end();
  process.exit();
});
