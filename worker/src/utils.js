/**
 * Renders page content and sets status
 * @param  {Object} connection from Express
 * @return {Function}
 */
export const render = conn => data => data.error
  ? conn.status(data.code).json({ error: data.error, ...data.msg })
  : conn.json(data);

/**
 * Reply from server with headers
 * @param  {Function} setHeaders set Headers for this reply
 * @return {Function}
 */
export function Reply(setHeaders = null) {
  return (result, conn) => {
    if (setHeaders) {
      setHeaders(conn);
    }
    return Promise.resolve(result).then(render(conn));
  };
}

export const setJsonHeader = conn => conn.setHeader('Content-Type', 'application/json');

/**
 * setInterval implementation from
 * http://thecodeship.com/web-development/alternative-to-javascript-evil-setinterval/
 */
export function interval(func, wait) {
  const interv = (w => () => {
    setTimeout(interv, w);
    try {
      func.call(null);
    } catch (e) {
      throw e.toString();
    }
  })(wait);
  setTimeout(interv, wait);
}
